# Envahisseurs de l'espace

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/envahisseurs.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/envahisseurs.git
```
